<?php
$host = "localhost";
$user = "root";
$password = "";
$dbname = "testdatabase";

$conn = mysqli_connect($host, $user, $password, $dbname);

if (!$conn) {
  die("Connection failed: " . mysqli_connect_error());
}

$id = $_GET['id'];

if (!$id) {
  echo "Aucun identifiant";
  exit;
}

$sql = "SELECT * FROM nobels WHERE id = $id";
$result = mysqli_query($conn, $sql);

if (mysqli_num_rows($result) == 0) {
  echo "Identifiant invalide";
  exit;
}

echo "<ul>";

while ($row = mysqli_fetch_assoc($result)) {
  echo "<li>Nom : " . $row['name'] . "</li>";
  echo "<li>Catégorie : " . $row['category'] . "</li>";
  echo "<li>Année : " . $row['year'] . "</li>";
  echo "<li>Motivation : " . $row['motivation'] . "</li>";
}

echo "</ul>";

mysqli_close($conn);

?>
</div>
</body>
</html>

    

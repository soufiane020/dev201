<?php

require_once "Model.php";


$infos = false;

if (isset($_GET["id"]) && $_GET["id"] === (string) (int) $_GET["id"] and $_GET["id"] > 0) {
    include_once "Model.php";
    $m = new Model();
    $infos = $m->get_nobel_prize_informations($_GET["id"]);
}

if ($infos === false) {
    require "begin.html";
    echo "<p>There is no such nobel prize!</p>";
    require "end.html";
    die();
}

require "begin.html";
?>


<?php echo "<h1>" . e($infos["name"]) . "</h1>";

echo "<ul><li> Year and place of birth: " . e($infos["birthdate"]) . " at " . e($infos["birthplace"]) . "</li> ";
echo "<li> County : " . e($infos["county"]) . "</li></ul>";
echo "<h2>Nobel Prize in " . e($infos["year"]) . " in the field of " . e($infos["category"]) . "</h2>";

echo "<p>" . e($infos["motivation"]) . "</p>";
?>

<?php require "end.html";
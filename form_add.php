<?php

require_once "home.php";

require_once "Model.php";
$m = Model::getModel();
$categories = $m->getCategories();

require "begin.html";
?>
<h1> Add a Nobel Prize </h1>

<form action = "add.php" method="post">
    <p> <label> Name: <input type="text" name="name"/> </label> 

    </p>
    <p> <label> Year: <input type="text" name="year"/> </label></p>
    <p> <label> Birth Date: <input type="text" name="birthdate"/></label> </p>
    <p> <label> Birth Place: <input type="text" name="birthplace"/> </label></p>
    <p> <label> County: <input type="text" name="county"/></label> </p>

    <p>
    <?php
    foreach ($categories as $v) {
        echo '<label> <input type="radio" name="category" value="' . e($v) . '"/>' . e(ucfirst($v)) . "</label>";
    }
    ?>
    </p>


    <textarea name="motivation" cols="70" rows="10"></textarea>
    <p>  <input type="submit" value="Add in database"/> </p>
</form>


<?php require "end.html"; ?>
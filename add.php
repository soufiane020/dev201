<?php

require_once "functions.php";

$infos = check_data();
// Si le formulaire a été soumis avec les bonnes valeurs
if ($infos !== false) {
    include_once "Model.php";
    $m = Model::getModel();
    $ok = $m->addNobelPrize($infos);
    if ($ok) {
        $message = "The nobel prize has been added.";
    } else {
        $message = "There was a problem when adding the nobel prize.";
    }
} else {
    $message = "There is no nobel prize to add.";
}

require "begin.html";
?>
<h1> Adding a nobel prize </h1>
<?php echo "<p>" . $message . "</p>"; ?>


<?php require "end.html"; ?>
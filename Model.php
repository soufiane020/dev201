<?php
class Model {
  public static $instance;
  public $conn;
  public function __construct() {
    $host = "localhost";
    $user = "root";
    $password = "";
    $dbname = "testdatabase";
    $this->conn = mysqli_connect($host, $user, $password, $dbname);


    if (!$this->conn) {
      throw new Exception("Connection failed: " . mysqli_connect_error());
    }

    mysqli_set_charset($this->conn, "utf8");

    mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
  }

 public static function get_instance() {
   if (!isset(self::$instance)) {
      self::$instance = new Model();
    }

    return self::$instance;
  }

  public static function getModel()
   {
        if (self::$instance === null) {
          self::$instance = new self();
        }
        return self::$instance;
    }

  public function get_last() {
    $sql = "SELECT * FROM nobels ORDER BY year DESC LIMIT 25";
    $result = mysqli_query($this->conn, $sql);
        echo "<table border='1'>";
        while($row = mysqli_fetch_assoc($result)){
        echo "<tr>";
        echo "<td>" . $row['name'] . "</td>";
        echo "<td>" . $row['category'] . "</td>";
        echo "<td>" . $row['year'] . "</td>";
        echo "</tr>";}
        echo "</table>";
    }


  public function get_nb_nobel_prizes() {
    $sql = "SELECT count(*) FROM nobels";
    $result = mysqli_query($this->conn, $sql);
    $row = mysqli_fetch_array($result);
    return $row[0];
  }


  public function get_nobel_prize_informations($id) {
    if (!is_int($id) || $id <= 0) {
      return false;
    }
    $db = new PDO('mysql:host=localhost;dbname=testdatabase', 'root', '');
    $stmt = $db->prepare('SELECT * FROM nobel_prizes WHERE id = :id');
    $stmt->bindValue(':id', $id, PDO::PARAM_INT);
    $stmt->execute();
    $prize = $stmt->fetch();
    return $prize ? $prize : false;
  


}
public function getCategories()
{
    $requete = $this->conn->prepare('SELECT * FROM categories');
    $requete->execute();
    $reponse = [];
    while ($ligne = $requete->fetch(PDO::FETCH_ASSOC)) {
        $reponse[] = $ligne['category'];
    }
    return $reponse;
}
}
?>
<?php

// Connexion à la base de données
$host = "localhost";
$user = "username";
$password = "password";
$dbname = "database_name";

$conn = mysqli_connect($host, $user, $password, $dbname);

if (!$conn) {
  die("Connection failed: " . mysqli_connect_error());
}

// Récupération de l'identifiant du prix nobel passé en paramètre dans l'URL
$id = $_GET['id'];

// Si l'identifiant est vide, affichage du message "Aucun identifiant"
if (!$id) {
  echo "Aucun identifiant";
  exit;
}

// Récupération des informations du prix nobel d'identifiant $id
$sql = "SELECT * FROM nobels WHERE id = $id";
$result = mysqli_query($conn, $sql);

// Si aucun prix nobel n'a été trouvé, affichage du message "Identifiant invalide"
if (mysqli_num_rows($result) == 0) {
  echo "Identifiant invalide";
  exit;
}

// Affichage des informations du prix nobel sous forme de liste non ordonnée
echo "<ul>";

while ($row = mysqli_fetch_assoc($result)) {
  echo "<li>Nom : " . $row['name'] . "</li>";
  echo "<li>Catégorie : " . $row['category'] . "</li>";
  echo "<li>Année : " . $row['year'] . "</li>";
  echo "<li>Motivation : " . $row['motivation'] . "</li>";
}

echo "</ul>";

mysqli_close($conn);
